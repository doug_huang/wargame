package com.doughuang;

import org.junit.*;
import static org.junit.Assert.*;
import java.util.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;

import com.doughuang.Card;

public class CardTest {

    private Card card;

    @Before
    public void setUp() {

        card = new Card(4, 13);
        System.out.println("@Before - setUp");
    }


    @Rule
    public ExpectedException thrown = ExpectedException.none();


    @Test
    public void testRankOutOfBoundsException() {

        thrown.expect(IndexOutOfBoundsException.class);
        thrown.expectMessage(containsString("rank is out of range"));

        int suit = 4;
        int rank = 15;

        Card c = new Card(suit,rank);
    }

    @Test
    public void testSuiteOutOfBoundsExceptionToo() {

        thrown.expect(IndexOutOfBoundsException.class);
        thrown.expectMessage(containsString("suit is out of range"));

        int suit = 5;
        int rank = 14;

        Card c = new Card(suit,rank);
    }

    @Test
    public void testCardSuite() {
        assertTrue(card.getSuit() == 4);
        System.out.println("@Test - testCardSuite");
    }

    @Test
    public void testCardRank() {
        assertTrue(card.getRank() == 13);
        System.out.println("@Test - testCardRank");
    }


}
